using UnityEngine;


public class CarCamera : MonoBehaviour
{
    public Transform target;
    public float followSpeed = 5f;
    public float rotationSpeed = 2f;

    public float distance = 6.34f;
    public float height = 2.55f;

    private void LateUpdate()
    {
        FollowCar();
    }

    private void FollowCar()
    {
        
        Vector3 targetPosition = target.position - target.forward * distance + Vector3.up * height; //la camara seguira a nuestro target(car)
        transform.position = Vector3.Lerp(transform.position, targetPosition, followSpeed * Time.deltaTime);
        Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position); // se calcula la rotacion
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
}

