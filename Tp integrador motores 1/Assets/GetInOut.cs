using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GetInOut : MonoBehaviour
{
    [SerializeField] GameObject Player = null!; // serialized field para ver las variables privadas en el inspector
    [SerializeField] GameObject car = null;
    [SerializeField] CarController CarController = null; //declarada como null pero es espera que no lo sea en play
    [SerializeField] CarCamera CarCamera = null;
    [SerializeField] Mouselook Mouselook = null;
    public bool inReach;

    public AudioListener playerAudioListener;
    public AudioListener carAudioListener;

    [SerializeField] KeyCode EnterExit = KeyCode.X;

    bool inCar = false;

    void Update()
    {
        if (Input.GetKeyDown(EnterExit))
        {
            if (!inCar) // inCar es falso, getIn
            {
                GetInCar();
            }
            else
            {
                GetOutCar();
            }
        }
    }

    void GetOutCar()
    {
        inCar = false;

        Player.SetActive(true);
        playerAudioListener.enabled = true;
        carAudioListener.enabled = false;

        Vector3 exitOffset = car.transform.TransformDirection(Vector3.up) * 2.5f + car.transform.TransformDirection(Vector3.left) * 1.5f;//donde esta el jugador al GetOut

        Player.transform.position = car.transform.position + exitOffset;

        CarController.enabled = false;
        CarCamera.enabled = false;

        Mouselook.enabled = true;
    }

    void GetInCar()
    {
        inCar = true;

        Player.SetActive(false);
        carAudioListener.enabled = true;
        playerAudioListener.enabled = false;
        GestorDeAudio.instancia.ReproducirSonido("CarEngine");
        


        CarController.enabled = true;
        CarCamera.enabled = true;
        Mouselook.enabled = false;
    }
}


