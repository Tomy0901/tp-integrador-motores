using UnityEngine;

public class BlinkingLight : MonoBehaviour
{
    public enum WaveForm { Sin, Tri, Sqr, Saw, Inv, Noise };
    public WaveForm waveForm = WaveForm.Sin;
    public float startValue = 0.0f;
    public float amplitude = 1.0f;
    public float phase = 0.0f;
    public float frequency = 0.5f;

    private Color originalColor;
    private Light lightSource;

    private void Start()
    {
        lightSource = GetComponent<Light>();
        originalColor = lightSource.color;
    }

    private void Update()
    {
        lightSource.color = originalColor * EvaluateWave();
    }

    private float EvaluateWave()
    {
        float x = (Time.time + phase) * frequency;
        float y;

        x = x - Mathf.Floor(x);

        switch (waveForm)
        {
            case WaveForm.Sin:
                y = Mathf.Sin(x * 2 * Mathf.PI);
                break;
            case WaveForm.Tri:
                y = (x < 0.5f) ? 4.0f * x - 1.0f : -4.0f * x + 3.0f;
                break;
            case WaveForm.Sqr:
                y = (x < 0.5f) ? 1.0f : -1.0f;
                break;
            case WaveForm.Saw:
                y = x;
                break;
            case WaveForm.Inv:
                y = 1.0f - x;
                break;
            case WaveForm.Noise:
                y = 1f - (Random.value * 2);
                break;
            default:
                y = 1.0f;
                break;
        }

        return (y * amplitude) + startValue;
    }
}

