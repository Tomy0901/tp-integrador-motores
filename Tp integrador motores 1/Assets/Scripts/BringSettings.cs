using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BringSettings : MonoBehaviour
{
    public GameObject setting;
    public bool issettingactive;
    public Slider sensitivitySlider;
    public Mouselook mouselookScript;

    void Start()
    {
        mouselookScript = GetComponent<Mouselook>();
        sensitivitySlider.value = mouselookScript.mouseSensitivity / 10;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleSettings();
        }

        
        mouselookScript.mouseSensitivity = sensitivitySlider.value * 10;
    }

    void ToggleSettings()
    {
        if (!issettingactive)
        {
            Pause();
        }
        else
        {
            Resume();
        }
    }

    void Pause()
    {
        setting.SetActive(true);
        issettingactive = true;
        mouselookScript.enabled = false; // desactivamos mover camara
        Cursor.lockState = CursorLockMode.None;//activamos cursor para el slider
    }

    void Resume()
    {
        setting.SetActive(false);
        issettingactive = false;
        mouselookScript.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
    }
}


