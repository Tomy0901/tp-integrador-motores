using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
    private float nextFire = 0f;
   
    public ParticleSystem muzzle;
    public Camera fpsCam;
    public GameObject impactEffect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextFire)
        {
            nextFire = Time.time + 1f / fireRate;
            Shoot();
            
        }
    }
    void Shoot()
    {
        muzzle.Play(); //partycle system playing

        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range)) //si el raycast golpea algo, debugea el nombre del objeto
        { 
            Debug.Log(hit.transform.name);
           

            Target target = hit.transform.GetComponent<Target>();
            if(target != null )//si el target no es null, llama a la funcion da�ar
            {
                target.TakeDamage(damage);
            }
          GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal)); // destruye la bala luego de 2 segundos
            Destroy(impactGO, 2f);
        }
       
    }
}

