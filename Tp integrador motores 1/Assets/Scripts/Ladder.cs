using UnityEngine;

public class Ladder : MonoBehaviour
{
    public Transform chController;
    bool inside = false;
    public float speedUpDown = 3.2f;
    public PlayerMovement FPSInput;

    void Start()
    {
        FPSInput = GetComponent<PlayerMovement>();
        inside = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Ladder")
        {
            FPSInput.enabled = false;
            inside = true;
            Debug.Log("Entered Ladder");
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Ladder")
        {
            FPSInput.enabled = true;
            inside = false;
            Debug.Log("Exited Ladder");
        }
    }

    void LateUpdate()
    {
        if (inside)
        {
            float verticalInput = Input.GetAxis("Vertical");

            if (verticalInput > 0)
            {
                chController.transform.position += Vector3.up * speedUpDown * Time.deltaTime;
                Debug.Log("Climbing Up");
            }
            else if (verticalInput < 0)
            {
                chController.transform.position += Vector3.down * speedUpDown * Time.deltaTime;
                Debug.Log("Climbing Down");
            }
        }
    }
}


