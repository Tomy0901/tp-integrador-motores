using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouselook : MonoBehaviour
{
    public float mouseSensitivity = 10f;
    public Slider slider;
    public Transform playerBody;

    float xRotation = 0f;

    void Start()
    {
        LoadMouseSensitivity();
        Cursor.lockState = CursorLockMode.Locked;
        UpdateSliderValue();
    }

    void Update()
    {
        UpdateMouseLook();
    }

    void UpdateMouseLook()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);// evita que nos rompamos el cuello al girar

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    public void AdjustSpeed(float newSpeed)
    {
        mouseSensitivity = newSpeed * 10;
        SaveMouseSensitivity();
    }

    void UpdateSliderValue()
    {
        slider.value = mouseSensitivity / 10;
    }

    void LoadMouseSensitivity()
    {
        mouseSensitivity = PlayerPrefs.GetFloat("currentSensitivity", 10); //cargamos la sensibilidad de las preferencias del player, sino es seteada a 10
    }

    void SaveMouseSensitivity()
    {
        PlayerPrefs.SetFloat("currentSensitivity", mouseSensitivity); //guarda la sensibilidad actual como pref
    }
}

