using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Windows.WebCam;

public class PlayerMovement : MonoBehaviour
{

    #region Variables
    public CharacterController controller;
    public Camera mainCamera;
    public Slider staminaSlider;

    public ParticleSystem jumpEffect;
    public Transform playerTransform;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    private int jumpCharges = 1;

    public float stamina = 100f;
    public float staminaUseRate = 10f;
    public float staminaRechargeRate = 5f;

    public float maxStamina = 100f;


    public float doubleJumpForce = 5f;
    private int jumpsRemaining;

    public int airSpeed = 10;
    public float sprintSpeed = 25;
    public float crouchSpeed = 3;

    public float startHeight;
    public float crouchHeight = 0.5f;

    private float runFov = 70;
    private float crouchFov = 50;
    private float normalFov = 60;
 

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;


    Vector3 velocity;
    bool isGrounded;
    bool isInAir;
    bool isSprinting;
    bool isCrouching;

    #endregion



    // Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("HotPursuit");
        startHeight = transform.localScale.y;
        

        stamina = maxStamina;
        staminaSlider.maxValue = maxStamina;
        staminaSlider.value = stamina;

        jumpsRemaining = jumpCharges;
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        isInAir = !isGrounded; 

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
            jumpsRemaining = jumpCharges;
        }

        float x = Input.GetAxis("Horizontal"); //input get
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        float currentSpeed = isGrounded ? speed : (isSprinting ? sprintSpeed : (isCrouching ? crouchSpeed : airSpeed));
        controller.Move(move * currentSpeed * Time.deltaTime);


        if (Input.GetKeyDown(KeyCode.Space) && jumpsRemaining > 0)
        {
            if (!isGrounded) // Si no estamos en el suelo, es un doble salto.
            {
                velocity.y = Mathf.Sqrt(doubleJumpForce * -2f * gravity);
                jumpsRemaining--; // Reducir la cantidad de saltos disponibles.
                airMovement(); // Aplicar movimiento adicional en el aire si es necesario.
                GestorDeAudio.instancia.ReproducirSonido("Recharge");
                jumpEffect.Play();
               
            }
            else if (stamina >=10)// Si estamos en el suelo, es un salto normal.
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
                airMovement();
                GestorDeAudio.instancia.ReproducirSonido("Recharge");
                

}
        }

            if (Input.GetKeyDown(KeyCode.C) && isGrounded)
        {
            crouch();
            mainCamera.fieldOfView = crouchFov; //cambiamos el FOV para darle un efecto y distinguir cuando nos agachamos
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
           exitCrouch();
           mainCamera.fieldOfView = normalFov;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && isGrounded && isCrouching == false) 
        {
            theSprint();
           
            Debug.Log("Corre");
        }
        if (Input.GetKeyUp(KeyCode.LeftShift) && isGrounded)
        {
            exitTheSprint();
           
        }
            velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if (stamina <= 0)
        {
            exitTheSprint(); // checkeando la stamina en el update, cuando la stamina llegue a 0, automaticamente deja de sprintar, por eso es necesario checkearlo en la funcion y en el update
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            transform.localScale = new Vector3(transform.localScale.x * 2f, transform.localScale.y * 2f, transform.localScale.z * 2f);//hacer al personaje mas grande
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            transform.localScale = new Vector3(transform.localScale.x * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z * 0.5f);//hacer al personaje chiquito
        }
        


    }
    private void FixedUpdate() // chequeamos la stamina aca para que cuando se nos acabe, dejemos de sprintar
    {
        if (isSprinting)
        {
            useStamina();
        }
        else
        {
            rechargeStamina();
        }
        updateStaminaUI();

    }

    #region funciones
    private void airMovement() // nos movemos mas rapidos mientras estamos en el aire
    {
        if (isInAir) 
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            Vector3 move = transform.right * x + transform.forward * z;
            controller.Move(move * airSpeed * Time.deltaTime);
        }
    }

    private void crouch()
    {
        speed = crouchSpeed;
        isCrouching = true;
        transform.localScale = new Vector3(transform.localScale.x, 0.5f, transform.localScale.z); //doblamos al personaje en la mitad
        Debug.Log("agachate");
    }

    private void exitCrouch()
    {
        speed = 10;
        isCrouching = false;
        transform.localScale = new Vector3(transform.localScale.x, 1.0f, transform.localScale.z);
        Debug.Log("parate");
    }

    private void theSprint()
    {
        if (stamina > 10 && Input.GetKey(KeyCode.LeftShift)) //aumentamos velocidad al sprintar y cambiamos FOV
        {
            speed = sprintSpeed;
            isSprinting = true;
            mainCamera.fieldOfView = runFov;
            useStamina();
        }
        else if(stamina <= 0)
        {
            speed = 10;
            exitTheSprint();
        }
        
    }
    private void exitTheSprint()
    {
        speed = 10;
        isSprinting = false;
        mainCamera.fieldOfView = normalFov;
    }

    void useStamina()
    {
        stamina -= staminaUseRate * Time.fixedDeltaTime;
        stamina = Mathf.Clamp(stamina, 0f, 100f);
    }
    void rechargeStamina()
    {
        stamina += staminaRechargeRate * Time.fixedDeltaTime;
        stamina = Mathf.Clamp(stamina, 0f, 100f);
    }

    void updateStaminaUI()
    {
        staminaSlider.value = stamina;
    }
    private void OnTriggerEnter(Collider other) //collider para ganar
    {
        if (other.gameObject.CompareTag("Win"))
        {
            other.gameObject.SetActive(false);
            SceneManager.LoadScene("Finish");
        }
    }

    #endregion
}
