using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRun : MonoBehaviour
{
    #region variables
    [Header("WallRunning")]
    [SerializeField] LayerMask whatIsWall;
    [SerializeField] LayerMask whatIsGround;
    [SerializeField] float wallRunForce; //velocidad del wallrun
    [SerializeField] float maxWallRunTime;
    [SerializeField] float wallJumpUpForce;
    [SerializeField] float wallJumpSideForce;
    float wallRunTimer;

    [Header("Input")]
    [SerializeField] float horizontalInput;
    [SerializeField] float verticalInput;

    [Header("Detection")]
    [SerializeField] float wallCheckDistance = .5f;
    [SerializeField] float minJumpHeight = 1.5f;
    RaycastHit leftWallHit, rightWallHit;
    [SerializeField] bool wallLeft, wallRight;

    [Header("Exiting Wall")]
    bool exitingWall;
    float exitWallTime;
    [SerializeField] float exitWallTimer;
  


    [Header("References")]
    [SerializeField] Transform orientation;
    PlayerMovement PlayerMovement;
    CharacterController controller;

    public float gravity;


    #endregion


    void Start()
    {
        controller = GetComponent<CharacterController>();
        PlayerMovement = GetComponent<PlayerMovement>();
        exitWallTime = exitWallTimer;
    }

 

    // Update is called once per frame
    void Update()
    {
        CheckForWall();
        StateMachine();


    }

    private void FixedUpdate()
    {
        if (!PlayerMovement.enabled)
            WallRunMovement();
    }

    #region funciones

    void CheckForWall()
    {
        wallRight = Physics.Raycast(transform.position, orientation.right, out rightWallHit, wallCheckDistance, whatIsWall); //dispara un raycast para checkear si hay una pared al costado
        wallLeft = Physics.Raycast(transform.position, -orientation.right, out leftWallHit, wallCheckDistance, whatIsWall);
    }

    bool AboveGround()
    {
        return !Physics.Raycast(transform.position, Vector3.down, minJumpHeight, whatIsGround);//dispara raycast al suelo para chequear si el player esta en el suelo
    }

    void StateMachine()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if ((wallLeft || wallRight) && verticalInput > 0 && AboveGround() && !exitingWall) //chequea si el jugador esta en una pared, yendo para adelante, sobre el suelo y no esta saliendo de una pared, si todo se cumple comienza el wallrun 
        {
            if (PlayerMovement.enabled)
                StartWallRun();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                WallJump();
            }
        }
        else if (exitingWall) //si no se cumplen condiciones, comienza el timer de exit
        {
            if (!PlayerMovement.enabled)
                StopWallRun();

            if (exitWallTimer > 0)
                exitWallTimer -= Time.deltaTime;

            if (exitWallTimer <= 0)
            {
                exitingWall = false;

            }

        }
        else
        {
            if (!PlayerMovement.enabled)
                StopWallRun();
        }

    }

    void StartWallRun()
    {
        PlayerMovement.enabled = false; //desactiva el movimiento en el wall run
     
    }

    void WallRunMovement()//calcula movimiento del jugador y como se mueve por la pared
    {
        Vector3 wallNormal = wallRight ? rightWallHit.normal : leftWallHit.normal;

        Vector3 wallForward = Vector3.Cross(wallNormal, transform.up);

        if ((orientation.forward - wallForward).magnitude > (orientation.forward + wallForward).magnitude)
            wallForward = -wallForward;

        // Smoothly interpolate between current direction and wall forward direction
        Vector3 newDir = Vector3.Slerp(transform.forward, wallForward, wallRunForce * Time.fixedDeltaTime);
        controller.Move(newDir * wallRunForce * Time.fixedDeltaTime);

       
    }


    void StopWallRun()
    {
        PlayerMovement.enabled = true;

    }

    void WallJump() //inicia salto aplicando fuerza al Chcontroller
    {
        exitingWall = true;
        exitWallTimer = exitWallTime;
      

        Vector3 wallNormal = wallRight ? rightWallHit.normal : leftWallHit.normal;
        Vector3 forceToApply = transform.up * wallJumpUpForce + wallNormal * wallJumpSideForce; 

        controller.Move(forceToApply.normalized);

    }

 
    #endregion
}

