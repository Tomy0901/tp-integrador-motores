using UnityEngine;
using UnityEngine.SceneManagement;

public class reloadScene : MonoBehaviour
{
    public Transform playerTransform;
    public float minYPosition = -10f;

  

   

    void Update()
    {
        // Check the condition for reloading the scene
        if (Input.GetKeyDown(KeyCode.R) || (playerTransform.position.y < minYPosition)) // si R o jugador se cae a cierto punto (-10Y) se reinicia escena activa
        {
            // Reload the active scene
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}

